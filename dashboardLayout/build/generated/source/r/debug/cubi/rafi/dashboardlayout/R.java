/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * gradle plugin from the resource data it found. It
 * should not be modified by hand.
 */
package cubi.rafi.dashboardlayout;

public final class R {
    public static final class dimen {
        public static int body_padding_large = 0x7f080001;
        public static int body_padding_medium = 0x7f080002;
        public static int speaker_image_padding = 0x7f080003;
        public static int speaker_image_size = 0x7f080004;
        public static int text_size_large = 0x7f080005;
        public static int text_size_medium = 0x7f080006;
        public static int text_size_small = 0x7f080007;
        public static int text_size_xlarge = 0x7f080008;
        public static int vendor_image_size = 0x7f080009;
    }
    public static final class drawable {
        public static int grid_menu_background = 0x7f090001;
        public static int grid_menu_style = 0x7f090002;
    }
    public static final class style {
        public static int DashboardButton = 0x7f160001;
    }
}
